import Model.GUI.AppWindow;
import Services.GUI.impl.AppWindowService;

public class App {


    public static void main(String[] args) {

        AppWindow appWindow = new AppWindow();
        AppWindowService appWindowService = new AppWindowService(appWindow);
        appWindow = appWindowService.showWindow();
    }
}