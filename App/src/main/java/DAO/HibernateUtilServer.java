package DAO;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtilServer {
    private static final SessionFactory sessionFactoryServer;


    static {
        try {
            sessionFactoryServer = new Configuration().configure("hibernateServer.cfg.xml").buildSessionFactory();
        } catch (Throwable ex) {
            System.err.println("Initial session factory creation failed: " + ex);
            throw new ExceptionInInitializerError(ex);//HibernateUtil.java:14
        }
    }

    public static SessionFactory getSessionFactoryServer() {
        return sessionFactoryServer;
    }

    public static void shutdown() {
        // Close caches and connection pools
        getSessionFactoryServer().close();
    }
}
