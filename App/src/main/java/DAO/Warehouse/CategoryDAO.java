package DAO.Warehouse;

import DAO.HibernateUtil;
import Model.Warehouse.Category;
import com.sun.istack.NotNull;
import org.hibernate.Hibernate;
import org.hibernate.Session;

import java.util.ArrayList;
import java.util.List;

//TODO Popups
public class CategoryDAO {

    public List<Category> getEnableCategories(){
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Category> categories = new ArrayList<>();
        try {
            session.beginTransaction();
            categories = session.createQuery("from Category where Deleted = 0").list();
        } catch (Exception e){
            e.printStackTrace();
        }finally {
            session.close();
        }
        return categories;
    }

    public Category getCategoryById(int idToFind) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Category category = new Category();
        Integer id = category.getId();
        try {
            session.beginTransaction();
            category = session.get(Category.class, id = idToFind);
            Hibernate.initialize(category);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            session.close();
        }
        return category;
    }

    public Category getCategoryByName(String name) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Category> categories = new ArrayList<Category>();
        try {
            session.beginTransaction();
            categories = session.createQuery("from Category where Name = '" + name + "'").list();
        } catch (Exception e){
            e.printStackTrace();
        }finally {
            session.close();
        }
        if(!categories.isEmpty()) {
            return categories.get(0);
        }else{
            return null;
        }
    }

    public boolean addNewCategory(String name) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Category category = new Category();
        category.setCategoryName(name);
        Integer saved = 0;
        try {
            session.beginTransaction();
            saved = (Integer)session.save(category);
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }finally {
            session.close();
            if(saved > 0)
                return true;
            return false;
        }
    }
    public boolean addNewCategory(@NotNull Category category) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        if(category.getCategoryName()!=null){
        try {
            session.beginTransaction();
            session.save(category);
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }finally {
            session.close();
            return true;
        }
        }else{
            return false;
        }
    }


    public boolean updateCategory(@NotNull Category category){
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            session.update(category);
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }finally {
            session.close();
            return true;
        }
    }

    public boolean deleteCategory(@NotNull Category category){
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            session.delete(category);
            session.flush() ;
        } catch (Exception e){
            e.printStackTrace();
            return false;
        }finally {
            session.close();
            return true;
        }
    }

}
