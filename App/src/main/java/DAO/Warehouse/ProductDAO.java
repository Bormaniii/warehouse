package DAO.Warehouse;

import DAO.HibernateUtil;
import Model.Warehouse.Product;
import com.sun.istack.NotNull;
import org.hibernate.Hibernate;
import org.hibernate.Session;

import java.util.ArrayList;
import java.util.List;

//TODO Popups
public class ProductDAO {

    public Product getProductById(int idToFind) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Product product = new Product();
        Integer id = product.getId();

        try {
            session.beginTransaction();
            product = session.get(Product.class, id = idToFind);
            Hibernate.initialize(product);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            session.close();
        }
        return product;
    }

    public boolean addNewProduct(String name, int categoryId, Double price) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Product product = new Product();
        product.setName(name);
        product.setCategoryId(categoryId);
        product.setPrice(price);

        Integer saved = 0;
        try {
            session.beginTransaction();
            saved = (Integer)session.save(product);
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }finally {
            session.close();
            if(saved > 0)
                return true;
            return false;
        }
    }

    public boolean addNewProduct(@NotNull Product product) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        if(product.getName()!=null && product.getCategoryId()!= null){
        try {
            session.beginTransaction();
            session.save(product);
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }finally {
            session.close();
            return true;
        }
        } else{
            return false;
        }
    }

    public boolean updateProduct(@NotNull Product product){
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            session.update(product);
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }finally {
            session.close();
            return true;
        }
    }

    public List<Product> getAllProducts(){
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Product> products = new ArrayList<Product>();
        try {
            session.beginTransaction();
            products = session.createQuery("from Product").list();
        } catch (Exception e){
            e.printStackTrace();
        }finally {
            session.close();
        }
        return products;
    }

    public List<Product> getProductsByCategoryId(int idToFind){
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Product> products = new ArrayList<Product>();
        try {
            session.beginTransaction();
            products = session.createQuery("from Product where CategoryId = " + idToFind + "order by name").list();
        } catch (Exception e){
            e.printStackTrace();
        }finally {
            session.close();
        }
        return products;
    }

    public Product getProductByName(String name){
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Product> products = new ArrayList<>();
        try {
            session.beginTransaction();
            products = session.createQuery("FROM Product WHERE Name = '" + name + "'").list();
        } catch (Exception e){
            e.printStackTrace();
        }finally {
            session.close();
        }
        if(!products.isEmpty()) {
            return products.get(0);
        }else{
            return null;
        }
    }

    public boolean deleteProduct(@NotNull Product product){
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            session.delete(product);
            session.flush() ;
        } catch (Exception e){
            e.printStackTrace();
            return false;
        }finally {
            session.close();
            return true;
        }
    }

    public boolean deleteProductByCategoryId(List<Product> products){
            Session session = HibernateUtil.getSessionFactory().openSession();
            try {
                for (Product product : products){
                    session.delete(product);
                }
                    session.flush();
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            } finally {
                session.close();
                return true;
            }
    }

}
