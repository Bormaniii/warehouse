package Model.GUI;

import Model.GUI.impl.SectionCategory;
import Model.GUI.impl.SectionProduct;

import javax.swing.*;
import java.awt.*;

public class AppWindow {
    static public Color mainColor = new Color(3, 102, 66);

    private MainMenu mainMenu = new MainMenu();

    private JFrame jFrame;
    private JPanel jPanel;
    private int appWidth = 650;
    private int appHeight = 500;

    public AppWindow(){
    }

    public MainMenu getMainMenu() {
        return mainMenu;
    }

    public void setMainMenu(MainMenu mainMenu) {
        this.mainMenu = mainMenu;
    }

    public JPanel getjPanel() {
        return jPanel;
    }

    public void setjPanel(JPanel jPanel) {
        this.jPanel = jPanel;
    }

    public JFrame getjFrame() {
        return jFrame;
    }

    public void setjFrame(JFrame jFrame) {
        this.jFrame = jFrame;
    }

    public int getAppWidth() {
        return appWidth;
    }

    public void setAppWidth(int appWidth) {
        this.appWidth = appWidth;
    }

    public int getAppHeight() {
        return appHeight;
    }

    public void setAppHeight(int appHeight) {
        this.appHeight = appHeight;
    }
}
