package Model.GUI;

import javax.swing.*;
import java.awt.*;

public class MainMenu{
    private FlowLayout flowLayout = new FlowLayout();;
    private JLabel logoTextArea = new JLabel("   Zarządzanie magazynem v0.9");
    private JButton viewProductsButton = new JButton("Przeglądaj produkty");
    private JButton manageProductsButton = new JButton("Zarządzaj Produktami");
    private JButton manageCategoriesButton = new JButton("Zarządzaj Kategoriami");

    public MainMenu(){
        logoTextArea.setFont(new Font("Arial", Font.BOLD, 30));
        manageCategoriesButton.setFont(new Font("Arial", Font.BOLD, 20));
        manageProductsButton.setFont(new Font("Arial", Font.BOLD, 20));
        viewProductsButton.setFont(new Font("Arial", Font.BOLD, 20));
    }

    public FlowLayout getFlowLayout() {
        return flowLayout;
    }

    public void setFlowLayout(FlowLayout flowLayout) {
        this.flowLayout = flowLayout;
    }

    public JLabel getLogoTextArea() {
        return logoTextArea;
    }

    public void setLogoTextArea(JLabel logoTextArea) {
        this.logoTextArea = logoTextArea;
    }

    public JButton getViewProductsButton() {
        return viewProductsButton;
    }

    public void setViewProductsButton(JButton viewProductsButton) {
        this.viewProductsButton = viewProductsButton;
    }

    public JButton getManageProductsButton() {
        return manageProductsButton;
    }

    public void setManageProductsButton(JButton manageProductsButton) {
        this.manageProductsButton = manageProductsButton;
    }

    public JButton getManageCategoriesButton() {
        return manageCategoriesButton;
    }

    public void setManageCategoriesButton(JButton manageCategoriesButton) {
        this.manageCategoriesButton = manageCategoriesButton;
    }
}
