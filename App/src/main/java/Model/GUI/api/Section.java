package Model.GUI.api;

import Model.Warehouse.Category;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public abstract class Section {
    private Color segmentColor = new Color(5, 179, 115);

    private JPanel categoryListPanel = new JPanel();

    private TitledBorder title = new TitledBorder("Title");

    private FlowLayout categoryLayout = new FlowLayout();

    private List<Category> categories = new ArrayList<>();
    private List<JRadioButton> categoryList = new ArrayList<>();
    private JLabel notFoundCategories = new JLabel("Brak Kategorii!");

    private JButton backButton = new JButton("Powrót");

    public Color getSegmentColor() {
        return segmentColor;
    }

    public void setSegmentColor(Color segmentColor) {
        this.segmentColor = segmentColor;
    }

    public JPanel getCategoryListPanel() {
        return categoryListPanel;
    }

    public void setCategoryListPanel(JPanel categoryListPanel) {
        this.categoryListPanel = categoryListPanel;
    }

    public TitledBorder getTitle() {
        return title;
    }

    public void setTitle(TitledBorder title) {
        this.title = title;
    }

    public FlowLayout getCategoryLayout() {
        return categoryLayout;
    }

    public void setCategoryLayout(FlowLayout categoryLayout) {
        this.categoryLayout = categoryLayout;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public List<JRadioButton> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<JRadioButton> categoryList) {
        this.categoryList = categoryList;
    }

    public JLabel getNotFoundCategories() {
        return notFoundCategories;
    }

    public void setNotFoundCategories(JLabel notFoundCategories) {
        this.notFoundCategories = notFoundCategories;
    }

    public JButton getBackButton() {
        return backButton;
    }

    public void setBackButton(JButton backButton) {
        this.backButton = backButton;
    }
}
