package Model.GUI.impl;

import Model.GUI.api.Section;
import Services.GUI.api.FocusTextField;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;

public class SectionCategory extends Section {
    private JPanel categoryPanel = new JPanel();
    private JPanel manageCategoryPanel = new JPanel();

    private GridLayout mainLayout = new GridLayout(4,1);
    private GridBagLayout categoryListLayout = new GridBagLayout();
    private FlowLayout manageCategoryLayout = new FlowLayout();

    private GridBagConstraints gbc = new GridBagConstraints();

    private JButton deleteCategoryButton = new JButton("Usuń zaznaczoną kategorię");
    private JTextField newCategoryInput = new FocusTextField("Nowa kategoria", 30);
    private JButton newCategoryButton = new JButton("Dodaj kategorię");

    public JPanel getCategoryPanel() {
        return categoryPanel;
    }

    public void setCategoryPanel(JPanel categoryPanel) {
        this.categoryPanel = categoryPanel;
    }

    public JPanel getManageCategoryPanel() {
        return manageCategoryPanel;
    }

    public void setManageCategoryPanel(JPanel manageCategoryPanel) {
        this.manageCategoryPanel = manageCategoryPanel;
    }

    public GridLayout getMainLayout() {
        return mainLayout;
    }

    public void setMainLayout(GridLayout mainLayout) {
        this.mainLayout = mainLayout;
    }

    public GridBagLayout getCategoryListLayout() {
        return categoryListLayout;
    }

    public void setCategoryListLayout(GridBagLayout categoryListLayout) {
        this.categoryListLayout = categoryListLayout;
    }

    public FlowLayout getManageCategoryLayout() {
        return manageCategoryLayout;
    }

    public void setManageCategoryLayout(FlowLayout manageCategoryLayout) {
        this.manageCategoryLayout = manageCategoryLayout;
    }

    public GridBagConstraints getGbc() {
        return gbc;
    }

    public void setGbc(GridBagConstraints gbc) {
        this.gbc = gbc;
    }

    public JButton getDeleteCategoryButton() {
        return deleteCategoryButton;
    }

    public void setDeleteCategoryButton(JButton deleteCategoryButton) {
        this.deleteCategoryButton = deleteCategoryButton;
    }

    public JTextField getNewCategoryInput() {
        return newCategoryInput;
    }

    public void setNewCategoryInput(JTextField newCategoryInput) {
        this.newCategoryInput = newCategoryInput;
    }

    public JButton getNewCategoryButton() {
        return newCategoryButton;
    }

    public void setNewCategoryButton(JButton newCategoryButton) {
        this.newCategoryButton = newCategoryButton;
    }
}
