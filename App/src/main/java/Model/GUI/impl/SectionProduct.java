package Model.GUI.impl;

import Model.GUI.api.Section;
import Model.Warehouse.Product;
import Services.GUI.api.FocusTextField;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

public class SectionProduct extends Section {
    private JPanel stockSegmentPanel = new JPanel();
    private JPanel managementSegmentPanel = new JPanel();
    private JPanel manageJRadioButtonsPanel = new JPanel();
    private JPanel manageProductsPanel = new JPanel();

    private GridLayout mainLayout = new GridLayout(4, 1);
    private GridBagLayout productStockLayout = new GridBagLayout();
    private GridLayout manageProductLayout = new GridLayout(1, 2);

    private GridBagConstraints gbc = new GridBagConstraints();

    private JComboBox<Product> productList = new JComboBox<>();
    private JLabel qtyLabel = new JLabel("Na stanie: 0");
    private JLabel changeQtyLabel = new JLabel("Wprowadź wartość: ");
    private JLabel priceLabel = new JLabel("Cena jednostkowa: 0,00zł");
    private JTextField changeQuantity = new FocusTextField("", 3);
    private JButton addQtyButton = new JButton("Wprowadź na magazyn");
    private JButton removeQtyButton = new JButton("Zdejmij z magazynu");

    //Inside ButtonGroup created in SectionProductService class
    private JRadioButton newProdSectionRadButton = new JRadioButton("Nowy produkt", true);
    private JRadioButton deleteProdSectionRadButton = new JRadioButton("Usuń produkt");

    private JComboBox<Product> manageProductList = new JComboBox<>();
    private JLabel manageQuantityLabel = new JLabel("Na stanie: 0");
    private JTextField manageNewProdInput = new FocusTextField("Nowy produkt", 20);
    private JTextField managePriceInput = new FocusTextField("Cena", 5);
    private JButton manageNewProdButton = new JButton("Dodaj nowy produkt");
    private JButton manageDeleteProdButton = new JButton("Usuń produkt");

    private Map<String,String> toReplace = new HashMap<String,String>();

    public SectionProduct() {
        this.toReplace.put(",",".");
        this.toReplace.put("zl","");
        this.toReplace.put("zł","");
    }

    public JPanel getStockSegmentPanel() {
        return stockSegmentPanel;
    }

    public void setStockSegmentPanel(JPanel stockSegmentPanel) {
        this.stockSegmentPanel = stockSegmentPanel;
    }

    public JPanel getManagementSegmentPanel() {
        return managementSegmentPanel;
    }

    public void setManagementSegmentPanel(JPanel managementSegmentPanel) {
        this.managementSegmentPanel = managementSegmentPanel;
    }

    public JPanel getManageJRadioButtonsPanel() {
        return manageJRadioButtonsPanel;
    }

    public void setManageJRadioButtonsPanel(JPanel manageJRadioButtonsPanel) {
        this.manageJRadioButtonsPanel = manageJRadioButtonsPanel;
    }

    public JPanel getManageProductsPanel() {
        return manageProductsPanel;
    }

    public void setManageProductsPanel(JPanel manageProductsPanel) {
        this.manageProductsPanel = manageProductsPanel;
    }

    public GridLayout getMainLayout() {
        return mainLayout;
    }

    public void setMainLayout(GridLayout mainLayout) {
        this.mainLayout = mainLayout;
    }

    public GridBagLayout getProductStockLayout() {
        return productStockLayout;
    }

    public void setProductStockLayout(GridBagLayout productStockLayout) {
        this.productStockLayout = productStockLayout;
    }

    public GridLayout getManageProductLayout() {
        return manageProductLayout;
    }

    public void setManageProductLayout(GridLayout manageProductLayout) {
        this.manageProductLayout = manageProductLayout;
    }

    public GridBagConstraints getGbc() {
        return gbc;
    }

    public void setGbc(GridBagConstraints gbc) {
        this.gbc = gbc;
    }

    public JComboBox<Product> getProductList() {
        return productList;
    }

    public void setProductList(JComboBox<Product> productList) {
        this.productList = productList;
    }

    public JLabel getQtyLabel() {
        return qtyLabel;
    }

    public void setQtyLabel(JLabel qtyLabel) {
        this.qtyLabel = qtyLabel;
    }

    public JLabel getChangeQtyLabel() {
        return changeQtyLabel;
    }

    public void setChangeQtyLabel(JLabel changeQtyLabel) {
        this.changeQtyLabel = changeQtyLabel;
    }

    public JLabel getPriceLabel() {
        return priceLabel;
    }

    public void setPriceLabel(JLabel priceLabel) {
        this.priceLabel = priceLabel;
    }

    public JTextField getChangeQuantity() {
        return changeQuantity;
    }

    public void setChangeQuantity(JTextField changeQuantity) {
        this.changeQuantity = changeQuantity;
    }

    public JButton getAddQtyButton() {
        return addQtyButton;
    }

    public void setAddQtyButton(JButton addQtyButton) {
        this.addQtyButton = addQtyButton;
    }

    public JButton getRemoveQtyButton() {
        return removeQtyButton;
    }

    public void setRemoveQtyButton(JButton removeQtyButton) {
        this.removeQtyButton = removeQtyButton;
    }

    public JRadioButton getNewProdSectionRadButton() {
        return newProdSectionRadButton;
    }

    public void setNewProdSectionRadButton(JRadioButton newProdSectionRadButton) {
        this.newProdSectionRadButton = newProdSectionRadButton;
    }

    public JRadioButton getDeleteProdSectionRadButton() {
        return deleteProdSectionRadButton;
    }

    public void setDeleteProdSectionRadButton(JRadioButton deleteProdSectionRadButton) {
        this.deleteProdSectionRadButton = deleteProdSectionRadButton;
    }

    public JComboBox<Product> getManageProductList() {
        return manageProductList;
    }

    public void setManageProductList(JComboBox<Product> manageProductList) {
        this.manageProductList = manageProductList;
    }

    public JLabel getManageQuantityLabel() {
        return manageQuantityLabel;
    }

    public void setManageQuantityLabel(JLabel manageQuantityLabel) {
        this.manageQuantityLabel = manageQuantityLabel;
    }

    public JTextField getManageNewProdInput() {
        return manageNewProdInput;
    }

    public void setManageNewProdInput(JTextField manageNewProdInput) {
        this.manageNewProdInput = manageNewProdInput;
    }

    public JTextField getManagePriceInput() {
        return managePriceInput;
    }

    public void setManagePriceInput(JTextField managePriceInput) {
        this.managePriceInput = managePriceInput;
    }

    public JButton getManageNewProdButton() {
        return manageNewProdButton;
    }

    public void setManageNewProdButton(JButton manageNewProdButton) {
        this.manageNewProdButton = manageNewProdButton;
    }

    public JButton getManageDeleteProdButton() {
        return manageDeleteProdButton;
    }

    public void setManageDeleteProdButton(JButton manageDeleteProdButton) {
        this.manageDeleteProdButton = manageDeleteProdButton;
    }

    public Map<String, String> getToReplace() {
        return toReplace;
    }

    public void setToReplace(Map<String, String> toReplace) {
        this.toReplace = toReplace;
    }
}