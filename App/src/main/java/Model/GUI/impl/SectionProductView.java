package Model.GUI.impl;

import Model.GUI.api.Section;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class SectionProductView extends Section {

    private GridLayout productsViewLayout = new GridLayout(3,1);

    private String[] columnNames = {"Nazwa produktu", "Ilość na stanie", "Cena"};
    private JScrollPane listScroller = new JScrollPane();
    private JTable productsTable = new JTable();
    private DefaultTableModel model = new DefaultTableModel();

    public SectionProductView(){
    }

    public GridLayout getProductsViewLayout() {
        return productsViewLayout;
    }

    public void setProductsViewLayout(GridLayout productsViewLayout) {
        this.productsViewLayout = productsViewLayout;
    }

    public String[] getColumnNames() {
        return columnNames;
    }

    public void setColumnNames(String[] columnNames) {
        this.columnNames = columnNames;
    }

    public JScrollPane getListScroller() {
        return listScroller;
    }

    public void setListScroller(JScrollPane listScroller) {
        this.listScroller = listScroller;
    }

    public JTable getProductsTable() {
        return productsTable;
    }

    public void setProductsTable(JTable productsTable) {
        this.productsTable = productsTable;
    }

    public DefaultTableModel getModel() {
        return model;
    }

    public void setModel(DefaultTableModel model) {
        this.model = model;
    }
}
