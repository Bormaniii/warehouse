package Model.Warehouse;


import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.swing.*;
import java.util.Date;

@Entity
@Table(name = "Categories", schema = "gd3kbmT67O", uniqueConstraints = {
        @UniqueConstraint(columnNames = "ID")})
public class Category extends JComboBox {
    private Integer id;
    private String categoryName;
    private boolean deleted = false;
    private Date modifyDate = new Date();

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId(){
        return id;
    }

    @Column(name = "Name", unique = true, nullable = false, length = 100)
    public String getCategoryName(){
        return categoryName;
    }

    @Column(name = "Deleted", columnDefinition = "boolean default false")
    public boolean isDeleted(){
        return deleted;
    }

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "modify_date")
    public Date getModifyDate(){return modifyDate;}

    public void setId(Integer id) {
        this.id = id;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    @Override
    public String toString() {
        return this.categoryName;
    }
}

