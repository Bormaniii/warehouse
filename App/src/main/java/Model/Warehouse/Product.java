package Model.Warehouse;

import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.swing.*;
import java.util.Date;

@Entity
@Table(name = "Products", schema = "gd3kbmT67O", uniqueConstraints = {
        @UniqueConstraint(columnNames = "ID")})
public class Product extends JComboBox {

    private Integer id;
    private String name;
    private Integer categoryId;
    private Integer value = 0;
    private Double price = 0.00;
    private boolean deleted = false;
    private Date modifyDate = new Date();

    public Product() {
    }


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    @Column(name = "Name", unique = true, nullable = false, length = 100)
    public String getName() {
        return name;
    }

    @Column(name = "CategoryId", nullable = false, columnDefinition = "int default 1")
    public Integer getCategoryId() {
        return categoryId;
    }

    @Column(name = "Value", columnDefinition = "int default 0")
    public Integer getValue() {
        return value;
    }

    @Column(name = "Price", columnDefinition = "Decimal(10,2) default 0.00")
    public Double getPrice() {
        return price;
    }

    @Column(name = "Deleted", columnDefinition = "boolean default false")
    public boolean isDeleted() {
        return deleted;
    }

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "modify_date")
    public Date getModifyDate() {
        return modifyDate;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }


    @Override
    public String toString() {
        return this.getName();
    }
}
