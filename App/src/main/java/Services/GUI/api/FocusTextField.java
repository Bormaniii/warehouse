package Services.GUI.api;

import javax.swing.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

public class FocusTextField extends JTextField {
    public FocusTextField(String text, Integer columns){
        super(text, columns);
    }
    {
        addFocusListener(new FocusListener() {

            @Override
            public void focusGained(FocusEvent e) {
                if(FocusTextField.this.getText().toLowerCase().equals("cena")){
                    FocusTextField.this.setText("0,00");
                }
                FocusTextField.this.select(0, getText().length());
            }

            @Override
            public void focusLost(FocusEvent e) {
                FocusTextField.this.select(0, 0);
            }
        });
    }
}
