package Services.GUI.api;

import Model.GUI.api.Section;
import Model.Warehouse.Category;
import Model.Warehouse.Product;
import Services.Warehouse.CategoryService;
import Services.Warehouse.ProductService;

import javax.swing.*;
import java.awt.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public abstract class SectionService implements ISectionService{
    private CategoryService categoryService = new CategoryService();

    public List<Category> getCategories(){
        CategoryService categoryService = new CategoryService();
        List<Category> categoryList = categoryService.getEnableCategories();
        return categoryList;
    }

    public GridBagConstraints setGBC(GridBagConstraints gbc, int gridx, int gridy){
        gbc.gridx = gridx;
        gbc.gridy = gridy;
        return gbc;
    }

    public Category getSelectedCategory(Section section){
        List<JRadioButton> categoryList = section.getCategoryList();
        for(int i=0;i<categoryList.size();i++){
            if(categoryList.get(i).isSelected()){
                String name = categoryList.get(i).getText();
                Category category = categoryService.getCategoryByName(name);
                return category;
            }
        }
        return null;
    }

    public JLabel setPrice(JLabel priceLabel, Double price){
        DecimalFormat df = new DecimalFormat("#0.00");

            if (price != null) {
                priceLabel.setText("Cena jednostkowa: " + df.format(price).replace(".", ",") + "zł");
            }else{
                priceLabel.setText("Cena jednostkowa: 0,00zł");
            }
        return priceLabel;
    }

    public JLabel setQuantity(JLabel quantity, Integer value){
        if (value != null) {
            quantity.setText("Na stanie: " + value.toString());
        }else{
            quantity.setText("Na stanie: 0");
        }
        return quantity;
    }

    public List<Product> getProductsByCategoryId(int idToFind){
        ProductService productService = new ProductService();

        return productService.getProductsByCategoryId(idToFind);
    }

    public void setCategories(Section section){
        section.setTitle(BorderFactory.createTitledBorder("Kategorie"));

        JPanel segmentPanel = section.getCategoryListPanel();
        segmentPanel.removeAll();
        segmentPanel.revalidate();
        segmentPanel.setLayout(section.getCategoryLayout());
        segmentPanel.setBorder(section.getTitle());
        segmentPanel.setBackground(section.getSegmentColor());
        segmentPanel.setPreferredSize(new Dimension(630,120));

        List<JRadioButton> jRadioButtons = new ArrayList<>();
        ButtonGroup buttonGroup = new ButtonGroup();

        List<Category> categories;
            categories = this.getCategories();
            section.setCategories(categories);

        for(int i = 0; i<categories.size();i++){
            jRadioButtons.add(new JRadioButton(categories.get(i).getCategoryName()));
            jRadioButtons.get(i).setBackground(section.getSegmentColor());
            buttonGroup.add(jRadioButtons.get(i));
            segmentPanel.add(jRadioButtons.get(i));
        }

        if(!jRadioButtons.isEmpty()) {
            jRadioButtons.get(0).setSelected(true);
        }else{
            segmentPanel.add(section.getNotFoundCategories());
        }

        section.setCategoryListPanel(segmentPanel);
        section.setCategoryList(jRadioButtons);
    }

}
