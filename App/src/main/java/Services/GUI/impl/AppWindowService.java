package Services.GUI.impl;

import DAO.HibernateUtilServer;
import Model.GUI.AppWindow;
import Services.GUI.impl.CategoryManage.SectionCategoryService;
import Services.GUI.impl.ProductsManage.SectionProductService;
import Services.GUI.impl.ProductsView.SectionProductViewService;
import org.hibernate.Session;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AppWindowService {
    private AppWindow appWindow = new AppWindow();

    public AppWindowService(AppWindow appWindow){
        this.appWindow = appWindow;
    }

    public static <T> T getSelectedItem(JComboBox<T> comboBox) {
        int index = comboBox.getSelectedIndex();
        return comboBox.getItemAt(index);
    }

    public AppWindow showWindow(){
        JFrame jFrame = new JFrame();
        JPanel jPanel = new JPanel();
        jPanel.setBackground(AppWindow.mainColor);

        jFrame.setSize(this.appWindow.getAppWidth(),this.appWindow.getAppHeight());
        jFrame.setMinimumSize(new Dimension(this.appWindow.getAppWidth()-100, this.appWindow.getAppHeight()-85));
        jFrame.setVisible(true);
        jFrame.add(jPanel);

        this.appWindow.setjFrame(jFrame);
        this.appWindow.setjPanel(jPanel);

        this.setMainMenuWindow();

        return appWindow;
    }

    public void setMainMenuWindow(){
        MainMenuService mainMenuService = new MainMenuService();
        this.appWindow.setjPanel(this.remoweAll(appWindow.getjPanel()));
        mainMenuService.setPanel(appWindow);
        this.appWindow.setjFrame(revalidatejFrame(appWindow.getjFrame()));
    }

    public void setSectionProductWindow(){
        SectionProductService sectionProductService = new SectionProductService();
        this.appWindow.setjPanel(this.remoweAll(appWindow.getjPanel()));
        sectionProductService.setPanel(appWindow);
        this.appWindow.setjFrame(revalidatejFrame(appWindow.getjFrame()));
    }

    public void setSectionCategoryWindow(){
        SectionCategoryService sectionCategoryService = new SectionCategoryService();
        this.appWindow.setjPanel(this.remoweAll(appWindow.getjPanel()));
        sectionCategoryService.setPanel(appWindow);
        this.appWindow.setjFrame(revalidatejFrame(appWindow.getjFrame()));
    }

    public void setSectionProductViewWindow(){
        SectionProductViewService sectionProductViewService = new SectionProductViewService();
        this.appWindow.setjPanel(this.remoweAll(appWindow.getjPanel()));
        sectionProductViewService.setPanel(appWindow);
        this.appWindow.setjFrame(revalidatejFrame(appWindow.getjFrame()));
    }

    private JFrame revalidatejFrame(JFrame jFrame){
        jFrame.revalidate();
        jFrame.repaint();
        return jFrame;
    }

    private JPanel remoweAll(JPanel jPanel){
        jPanel.removeAll();
        jPanel.repaint();
        return  jPanel;
    }
}
