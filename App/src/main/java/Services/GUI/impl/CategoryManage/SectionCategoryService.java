package Services.GUI.impl.CategoryManage;

import Model.GUI.AppWindow;
import Model.GUI.impl.SectionCategory;
import Model.Warehouse.Category;
import Model.Warehouse.Product;
import Services.GUI.api.SectionService;
import Services.GUI.impl.AppWindowService;
import Services.Warehouse.CategoryService;
import Services.Warehouse.ProductService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class SectionCategoryService extends SectionService {
    private SectionCategory sectionCategory = new SectionCategory();


    public void setPanel(AppWindow appWindow){
        JPanel jPanel = appWindow.getjPanel();
        jPanel.removeAll();
        jPanel.repaint();
        jPanel.setLayout(new BorderLayout(10,10));
        jPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));


        this.setCategories(sectionCategory);
        jPanel.add(this.sectionCategory.getCategoryListPanel(), BorderLayout.NORTH);

        JPanel segment = new JPanel(new BorderLayout());
        segment.setBackground(AppWindow.mainColor);

        this.setManageCategoryPanel(this.sectionCategory);
        segment.add(this.sectionCategory.getManageCategoryPanel(), BorderLayout.NORTH);
        jPanel.add(segment, BorderLayout.CENTER);

        jPanel.add(this.sectionCategory.getBackButton(), BorderLayout.SOUTH);

        appWindow.setjPanel(jPanel);

        this.setActions(appWindow, this.sectionCategory);
    }

    private void setManageCategoryPanel(SectionCategory sectionCategory){
        JPanel manageCategoryPanel = sectionCategory.getManageCategoryPanel();
        manageCategoryPanel.setLayout(new BorderLayout(20,25));
        manageCategoryPanel.setBackground(sectionCategory.getSegmentColor());

        sectionCategory.getDeleteCategoryButton().setPreferredSize(new Dimension(50,30));
        manageCategoryPanel.add(sectionCategory.getDeleteCategoryButton(), BorderLayout.PAGE_START);
        manageCategoryPanel.add(sectionCategory.getNewCategoryButton(), BorderLayout.WEST);
        manageCategoryPanel.add(sectionCategory.getNewCategoryInput(), BorderLayout.CENTER);
        manageCategoryPanel.add(Box.createRigidArea(new Dimension(5,1)), BorderLayout.SOUTH);

        sectionCategory.setManageCategoryPanel(manageCategoryPanel);
    }

    private void setActions(final AppWindow appWindow, final SectionCategory sectionCategory){
        final SectionCategoryService sectionCategoryService = new SectionCategoryService();
        final CategoryService categoryService = new CategoryService();
        final ProductService productService = new ProductService();

        sectionCategory.getDeleteCategoryButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Category selectedCategory = getSelectedCategory(sectionCategory);

                if(selectedCategory!=null) {
                    boolean deleted = false;
                    List<Product> products = productService.getProductsByCategoryId(selectedCategory.getId());

                    if(!products.isEmpty()){
                        for (Product product : products) {
                            product.setDeleted(true);
                            product.setValue(0);
                            deleted = productService.updateProduct(product);
                        }
                    }else{
                        deleted=true;
                    }

                    if (deleted) {
                        selectedCategory.setDeleted(true);
                        deleted = categoryService.updateCategory(selectedCategory);
                        if(deleted) {
                            sectionCategoryService.setCategories(sectionCategory);
                        } else {
                            JOptionPane.showMessageDialog(null, "Wystąpił błąd podczas usuwania Kategorii. \nSkonsultuj się z Supportem.");
                            sectionCategoryService.setCategories(sectionCategory);
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "Wystąpił błąd podczas usuwania Produktów należących do Kategorii. \nSkonsultuj się z Supportem.");
                        sectionCategoryService.setCategories(sectionCategory);
                    }
                }else{
                    JOptionPane.showMessageDialog(null, "Nie ma więcej Kategorii do usunięcia!");
                }
            }
        });

        sectionCategory.getNewCategoryButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String categoryName = sectionCategory.getNewCategoryInput().getText();
                boolean added = false;

                if(!categoryName.isEmpty() && !categoryName.equals("")) {
                    Category category = categoryService.getCategoryByName(categoryName);

                    if(category==null) {
                        added = categoryService.addNewCategory(categoryName);
                    }else if(category.isDeleted()){
                        category.setDeleted(false);
                        added = categoryService.updateCategory(category);
                    }else{
                        JOptionPane.showMessageDialog(null, "Dana Kategoria już istnieje.");
                        return;
                    }

                    if (added) {
                        sectionCategoryService.setCategories(sectionCategory);
                    } else {
                        JOptionPane.showMessageDialog(null, "Wystąpił błąd podczas dodawania Kategorii. \nSprawdź, czy dana kategoria już nie istnieje lub skonsultuj się z Supportem.");
                    }
                }else{
                    JOptionPane.showMessageDialog(null, "Pole jest puste!");
                }
            }
        });

        sectionCategory.getBackButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AppWindowService appWindowService = new AppWindowService(appWindow);
                appWindowService.setMainMenuWindow();
            }
        });
    }
}
