package Services.GUI.impl;

import Model.GUI.AppWindow;
import Model.GUI.MainMenu;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainMenuService {
    private MainMenu mainMenu = new MainMenu();

    public void setPanel(AppWindow appWindow) {
        this.mainMenu = appWindow.getMainMenu();

        JPanel jPanel = appWindow.getjPanel();
        jPanel.setLayout(new BoxLayout(jPanel, BoxLayout.PAGE_AXIS));
        jPanel.add(Box.createRigidArea(new Dimension(30,20)));
        jPanel.add(mainMenu.getLogoTextArea());
        jPanel.add(Box.createRigidArea(new Dimension(0,100)));
        jPanel.add(mainMenu.getManageProductsButton());
        jPanel.add(Box.createRigidArea(new Dimension(0,30)));
        jPanel.add(mainMenu.getManageCategoriesButton());
        jPanel.add(Box.createRigidArea(new Dimension(0,30)));
        jPanel.add(mainMenu.getViewProductsButton());

        appWindow.setjPanel(jPanel);
        appWindow.setMainMenu(mainMenu);

        this.setActions(appWindow, mainMenu);
    }

    private void setActions(final AppWindow appWindow, final MainMenu mainMenu) {
        mainMenu.getManageProductsButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                AppWindowService appWindowService = new AppWindowService(appWindow);
                appWindowService.setSectionProductWindow();
            }
        });

        mainMenu.getManageCategoriesButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AppWindowService appWindowService = new AppWindowService(appWindow);
                appWindowService.setSectionCategoryWindow();
            }
        });

        mainMenu.getViewProductsButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AppWindowService appWindowService = new AppWindowService(appWindow);
                appWindowService.setSectionProductViewWindow();
            }
        });

        appWindow.getjFrame().addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                System.exit(0);
            }
        });
    }


}
