package Services.GUI.impl.ProductsManage;

import Model.GUI.AppWindow;
import Model.GUI.impl.SectionProduct;
import Model.Warehouse.Category;
import Model.Warehouse.Product;
import Services.GUI.impl.AppWindowService;
import Services.Warehouse.ProductService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SectionProductManagementService {
    SectionProductService sectionProductService = new SectionProductService();

    public JPanel setSegmentPanel(JPanel mainPanel, SectionProduct sectionProduct) {
        JPanel segmentPanel = sectionProduct.getManagementSegmentPanel();
        sectionProduct.setTitle(BorderFactory.createTitledBorder("Dodaj/Usuń nowy Produkt"));
        segmentPanel.setLayout(new GridLayout(2, 1));
        segmentPanel.setBorder(sectionProduct.getTitle());
        segmentPanel.setBackground(sectionProduct.getSegmentColor());

        JPanel manageJRadioButtonsPanel = sectionProduct.getManageJRadioButtonsPanel();
        manageJRadioButtonsPanel.setLayout(sectionProduct.getManageProductLayout());


        ButtonGroup buttonGroup = new ButtonGroup();
        sectionProduct.getNewProdSectionRadButton().setBackground(sectionProduct.getSegmentColor());
        sectionProduct.getDeleteProdSectionRadButton().setBackground(sectionProduct.getSegmentColor());
        buttonGroup.add(sectionProduct.getNewProdSectionRadButton());
        buttonGroup.add(sectionProduct.getDeleteProdSectionRadButton());

        manageJRadioButtonsPanel.add(sectionProduct.getNewProdSectionRadButton());
        manageJRadioButtonsPanel.add(sectionProduct.getDeleteProdSectionRadButton());

        int chosenSection = checkManageSection(sectionProduct.getNewProdSectionRadButton(), sectionProduct.getDeleteProdSectionRadButton());
        //TODO as above
        JPanel manageProductsPanel = sectionProduct.getManageProductsPanel();
        manageProductsPanel = this.setManageProductSection(manageProductsPanel, chosenSection, sectionProduct);

        segmentPanel.add(manageJRadioButtonsPanel);
        segmentPanel.add(manageProductsPanel);
        mainPanel.add(segmentPanel);

        sectionProduct.setManagementSegmentPanel(segmentPanel);
        sectionProduct.setManageJRadioButtonsPanel(manageJRadioButtonsPanel);
        sectionProduct.setManageProductsPanel(manageProductsPanel);

        this.setActions(sectionProduct);

        return mainPanel;
    }

    private JPanel setManageProductSection(JPanel manageProductsPanel, int chosenSection, SectionProduct sectionProduct) {
        Category selectedCategory = this.sectionProductService.getSelectedCategory(sectionProduct);
        if (selectedCategory == null) {
            sectionProduct.getNewProdSectionRadButton().setEnabled(false);
            sectionProduct.getDeleteProdSectionRadButton().setEnabled(false);
        } else {
            manageProductsPanel.removeAll();
            manageProductsPanel.repaint();
            manageProductsPanel.revalidate();
            manageProductsPanel.setLayout(sectionProduct.getCategoryLayout());
            manageProductsPanel.setBackground(sectionProduct.getSegmentColor());

            sectionProduct.getNewProdSectionRadButton().setEnabled(true);
            sectionProduct.getDeleteProdSectionRadButton().setEnabled(true);

            switch (chosenSection) {
                case 1:
                    manageProductsPanel.add(sectionProduct.getManageNewProdInput());
                    manageProductsPanel.add(sectionProduct.getManagePriceInput());
                    manageProductsPanel.add(sectionProduct.getManageNewProdButton());
                    break;
                case 0:
                    JComboBox<Product> productNames = sectionProduct.getManageProductList();
                    if (productNames.getItemCount() == 0) {
                        productNames = this.sectionProductService.setProductNamesList(productNames, selectedCategory.getId());
                    }
                    sectionProduct.setManageProductList(productNames);
                    JLabel quantity = sectionProduct.getManageQuantityLabel();
                    manageProductsPanel.add(productNames);
                    manageProductsPanel.add(quantity);
                    manageProductsPanel.add(sectionProduct.getManageDeleteProdButton());
                    break;
            }
        }
        return manageProductsPanel;
    }

    private int checkManageSection(JRadioButton newProd, JRadioButton delProd) {
        if (newProd.isSelected()) {
            return 1;
        } else if (delProd.isSelected()) {
            return 0;
        } else {
            return -1;
        }
    }

    private void setActions(final SectionProduct sectionProduct) {
        final ProductService productService = new ProductService();

        sectionProduct.getNewProdSectionRadButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JPanel manageProductsPanel = sectionProduct.getManageProductsPanel();
                int chosenSection = checkManageSection(sectionProduct.getNewProdSectionRadButton(), sectionProduct.getDeleteProdSectionRadButton());
                manageProductsPanel = setManageProductSection(manageProductsPanel, chosenSection, sectionProduct);
                sectionProduct.setManageProductsPanel(manageProductsPanel);
            }
        });

        sectionProduct.getDeleteProdSectionRadButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JPanel manageProductsPanel = sectionProduct.getManageProductsPanel();
                int chosenSection = checkManageSection(sectionProduct.getNewProdSectionRadButton(), sectionProduct.getDeleteProdSectionRadButton());
                manageProductsPanel = setManageProductSection(manageProductsPanel, chosenSection, sectionProduct);
                sectionProduct.setManageProductsPanel(manageProductsPanel);

                JLabel manageQuantity = sectionProduct.getManageQuantityLabel();
                Product product = new Product();

                if (sectionProduct.getManageProductList().getSelectedItem() != null) {
                    product = AppWindowService.getSelectedItem(sectionProduct.getManageProductList());
                }
                manageQuantity = sectionProductService.setQuantity(manageQuantity, product.getValue());
                sectionProduct.setManageQuantityLabel(manageQuantity);
            }
        });

        sectionProduct.getManageProductList().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JLabel manageQuantity = sectionProduct.getManageQuantityLabel();
                Product product = new Product();
                if (sectionProduct.getManageProductList().getSelectedItem() != null) {
                    product = AppWindowService.getSelectedItem(sectionProduct.getManageProductList());
                }
                manageQuantity = sectionProductService.setQuantity(manageQuantity, product.getValue());
                sectionProduct.setManageQuantityLabel(manageQuantity);
            }
        });

        sectionProduct.getManageNewProdButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Category category = sectionProductService.getSelectedCategory(sectionProduct);
                Double price = 0.0;
                String convertedPrice = sectionProductService.convertPrice(sectionProduct.getManagePriceInput().getText());
                String productName = sectionProduct.getManageNewProdInput().getText();

                //Check if product already exists in DB
                Product product = productService.getProductByName(productName);
                sectionProduct.getManagePriceInput().setText("Cena");

                try {
                    price = Double.parseDouble(convertedPrice);
                } catch (Exception c) {
                    JOptionPane.showMessageDialog(null, "Wprowadź poprawną cenę w formacie \"0,00\"");
                    return;
                }
                if(price<0){
                    JOptionPane.showMessageDialog(null, "Wprowadź wartość dodatnią.");
                    return;
                }

                boolean added;
                if (product == null) {
                    added = productService.addNewProduct(productName, category.getId(), price);
                } else if (product.isDeleted()) {
                    product.setDeleted(false);
                    product.setCategoryId(category.getId());
                    product.setPrice(price);
                    added = productService.updateProduct(product);
                } else {
                    JOptionPane.showMessageDialog(null, "Dany produkt już istnieje.");
                    return;
                }

                if (added) {
                    JComboBox<Product> productList = sectionProduct.getProductList();
                    productList.removeAllItems();
                    productList = sectionProductService.setProductNamesList(productList, category.getId());
                    Integer newProdIndex = sectionProductService.getNewProdIndex(productList,productName);
                    productList.setSelectedIndex(newProdIndex);
                    sectionProduct.setProductList(productList);
                } else {
                    JOptionPane.showMessageDialog(null, "Wystąpił błąd podczas dodawania produktu. \nSkonsultuj się z Supportem.");
                }
            }
        });

        sectionProduct.getManageDeleteProdButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JComboBox<Product> productList = sectionProduct.getProductList();
                Product product = new Product();
                if (sectionProduct.getManageProductList().getSelectedItem() != null) {
                    product = AppWindowService.getSelectedItem(sectionProduct.getManageProductList());
                    product.setDeleted(true);
                    product.setValue(0);
                }

                boolean deleted = productService.updateProduct(product);

                if (deleted) {
                    Category selectedCategory = sectionProductService.getSelectedCategory(sectionProduct);
                    JComboBox<Product> manageProductList = sectionProduct.getManageProductList();
                    manageProductList.removeAllItems();
                    productList.removeAllItems();

                    manageProductList = sectionProductService.setProductNamesList(manageProductList, selectedCategory.getId());
                    sectionProduct.setManageProductList(manageProductList);
                    productList = sectionProductService.setProductNamesList(productList, selectedCategory.getId());
                    sectionProduct.setProductList(productList);
                    JOptionPane.showMessageDialog(null, "Produkt został usunięty pomyślnie");
                } else {

                    JOptionPane.showMessageDialog(null, "Nie udało się usunąć produktu. Spróbuj ponownie lub skonsultuj się z Supportem.");
                }
            }
        });
    }
}
