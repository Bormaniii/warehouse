package Services.GUI.impl.ProductsManage;

import Model.GUI.AppWindow;
import Model.GUI.impl.SectionProduct;
import Model.Warehouse.Category;
import Model.Warehouse.Product;
import Services.GUI.api.SectionService;
import Services.GUI.impl.AppWindowService;
import Services.Warehouse.ProductService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SectionProductService extends SectionService {
    private SectionProduct sectionProduct = new SectionProduct();

    public void setPanel(AppWindow appWindow){
        JPanel jPanel = appWindow.getjPanel();
        jPanel.removeAll();
        jPanel.repaint();
        jPanel.setLayout(new BorderLayout(10,10));
        jPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));

        this.setCategories(this.sectionProduct);
        jPanel.add(this.sectionProduct.getCategoryListPanel(), BorderLayout.NORTH);

        SectionProductStockService sectionProductStockService = new SectionProductStockService();
        SectionProductManagementService sectionProductManagementService = new SectionProductManagementService();

        JPanel segments = new JPanel(new GridLayout(2,1));
        segments = sectionProductStockService.setSegmentPanel(segments, this.sectionProduct);
        segments = sectionProductManagementService.setSegmentPanel(segments, this.sectionProduct);
        jPanel.add(segments, BorderLayout.CENTER);

        jPanel.add(this.sectionProduct.getBackButton(), BorderLayout.SOUTH);

        appWindow.setjPanel(jPanel);

        this.setActions(appWindow, this.sectionProduct);
    }

    public JComboBox<Product> setProductNamesList(JComboBox<Product> productsNames, int selectedCategory){
        for(Product product: this.getProductsByCategoryId(selectedCategory)){
            if(!product.isDeleted())
            productsNames.addItem(product);
        }

        productsNames.setBackground(Color.white);
        if(productsNames.getItemCount()>0)
        productsNames.setSelectedIndex(0);

        return productsNames;
    }

    public String convertPrice(String price){
        for (Map.Entry<String, String> fieldToReplace : this.sectionProduct.getToReplace().entrySet())
            price = price.replace(fieldToReplace.getKey(), fieldToReplace.getValue());
        return price;
    }

    public Integer getNewProdIndex(JComboBox<Product> productList, String productName){
        for(int i=0;i<productList.getItemCount();i++){
               if(productList.getItemAt(i).getName().equals(productName))
                   return i;
        }
        return 0;
    }

    private void setActions(final AppWindow appWindow, final SectionProduct sectionProduct){
        final SectionProductService sectionProductService = new SectionProductService();

        for(JRadioButton category: sectionProduct.getCategoryList()){
            category.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    Category selectedCategory = getSelectedCategory(sectionProduct);
                    JComboBox<Product> productList = sectionProduct.getProductList();
                    productList.removeAllItems();
                    JComboBox<Product> manageProductList = sectionProduct.getManageProductList();
                    manageProductList.removeAllItems();

                    productList = sectionProductService.setProductNamesList(productList , selectedCategory.getId());
                    manageProductList = sectionProductService.setProductNamesList(manageProductList, selectedCategory.getId());

                    sectionProduct.setProductList(productList);
                    sectionProduct.setManageProductList(manageProductList);
                }
            });
        }

        sectionProduct.getBackButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AppWindowService appWindowService = new AppWindowService(appWindow);
                appWindowService.setMainMenuWindow();
            }
        });
    }

}
