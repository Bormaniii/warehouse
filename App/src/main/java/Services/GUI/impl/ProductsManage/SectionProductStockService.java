package Services.GUI.impl.ProductsManage;

import Model.GUI.AppWindow;
import Model.GUI.impl.SectionProduct;
import Model.Warehouse.Category;
import Model.Warehouse.Product;
import Services.GUI.impl.AppWindowService;
import Services.Warehouse.ProductService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SectionProductStockService {
    private SectionProductService sectionProductService = new SectionProductService();

    public JPanel setSegmentPanel(JPanel mainPanel, SectionProduct sectionProduct){
        sectionProduct.setTitle(BorderFactory.createTitledBorder("Zarządzanie stanem"));

        JPanel segmentPanel = sectionProduct.getStockSegmentPanel();
        segmentPanel.setLayout(sectionProduct.getProductStockLayout());
        segmentPanel.setBorder(sectionProduct.getTitle());
        segmentPanel.setBackground(sectionProduct.getSegmentColor());
        GridBagConstraints gbc = sectionProduct.getGbc();

        segmentPanel = this.setProductNamesList(segmentPanel,gbc, sectionProduct);

        this.sectionProductService.setGBC(gbc,2,1);
        segmentPanel.add(sectionProduct.getQtyLabel(),gbc);
        this.sectionProductService.setGBC(gbc,4,1);
        segmentPanel.add(sectionProduct.getPriceLabel(),gbc);
        this.sectionProductService.setGBC(gbc,0,2);
        gbc.gridwidth=1;
        segmentPanel.add(sectionProduct.getChangeQtyLabel(),gbc);
        this.sectionProductService.setGBC(gbc,1,2);
        segmentPanel.add(sectionProduct.getChangeQuantity(), gbc);
        gbc.ipady=20;
        this.sectionProductService.setGBC(gbc,3,2);
        segmentPanel.add(sectionProduct.getAddQtyButton(), gbc);
        this.sectionProductService.setGBC(gbc,4,2);
        segmentPanel.add(sectionProduct.getRemoveQtyButton(),gbc);
        gbc.ipady=0;

        sectionProduct.setStockSegmentPanel(segmentPanel);
        mainPanel.add(segmentPanel);

        this.setActions(sectionProduct);

        return mainPanel;
    }

    private JPanel setProductNamesList(JPanel stockMovementPanel, GridBagConstraints gbc, SectionProduct sectionProduct){
        Category selectedCategory = this.sectionProductService.getSelectedCategory(sectionProduct);

        JComboBox<Product> productsNames = sectionProduct.getProductList();
        productsNames.removeAllItems();
        if(selectedCategory!=null) {
            productsNames = this.sectionProductService.setProductNamesList(productsNames, selectedCategory.getId());

            JLabel quantity = sectionProduct.getQtyLabel();
            if(productsNames.getItemAt(0)!=null)
                quantity = sectionProductService.setQuantity(quantity, productsNames.getItemAt(0).getValue());
            else
                quantity = sectionProductService.setQuantity(quantity,0);
            sectionProduct.setQtyLabel(quantity);
        }

        this.sectionProductService.setGBC(gbc,0,1);
        gbc.gridwidth=2;
        stockMovementPanel.add(productsNames, gbc);

        sectionProduct.setProductList(productsNames);

        return stockMovementPanel;
}

    private void setActions(final SectionProduct sectionProduct){

        sectionProduct.getProductList().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JLabel quantity = sectionProduct.getQtyLabel();
                JLabel price = sectionProduct.getPriceLabel();
                Product product = new Product();
                if(sectionProduct.getProductList().getSelectedItem()!=null) {
                    product = AppWindowService.getSelectedItem(sectionProduct.getProductList());
                }
                quantity = sectionProductService.setQuantity(quantity, product.getValue());
                price = sectionProductService.setPrice(price,product.getPrice());
                sectionProduct.setQtyLabel(quantity);
                sectionProduct.setPriceLabel(price);
            }
        });

        sectionProduct.getAddQtyButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JLabel quantity = sectionProduct.getQtyLabel();
                JTextField valueToAdd = sectionProduct.getChangeQuantity();
                try {
                    Product product = AppWindowService.getSelectedItem(sectionProduct.getProductList());
                    int value = product.getValue() + Integer.parseInt(valueToAdd.getText());
                    product.setValue(value);

                    ProductService productService = new ProductService();
                    boolean updated = productService.updateProduct(product);

                    if(updated) {
                        valueToAdd.setText("");
                        sectionProduct.setChangeQuantity(valueToAdd);

                        quantity = sectionProductService.setQuantity(quantity, product.getValue());
                        sectionProduct.setQtyLabel(quantity);
                    }else{
                        JOptionPane.showMessageDialog(null, "Nie udało się zmienić stanu magazynu. Skontaktuj się z Supportem.");
                    }
                }catch (Exception c){
                    JOptionPane.showMessageDialog(null, "Wprowadź Proszę liczbę całkowitą oraz wybierz produkt!");
                }
            }
        });

        sectionProduct.getRemoveQtyButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JLabel quantity = sectionProduct.getQtyLabel();
                JTextField valueToRemove = sectionProduct.getChangeQuantity();

                try {
                    Product product = AppWindowService.getSelectedItem(sectionProduct.getProductList());
                    int value = product.getValue() - Integer.parseInt(valueToRemove.getText());
                    if(value<0){
                        value = 0;
                    }

                    product.setValue(value);
                    ProductService productService = new ProductService();
                    boolean updated = productService.updateProduct(product);

                    if(updated) {
                        valueToRemove.setText("");
                        sectionProduct.setChangeQuantity(valueToRemove);

                        quantity = sectionProductService.setQuantity(quantity, product.getValue());
                        sectionProduct.setQtyLabel(quantity);
                    }else{
                        JOptionPane.showMessageDialog(null, "Nie udało się zmienić stanu magazynu. Skontaktuj się z Supportem.");
                    }
                }catch (Exception c){
                    JOptionPane.showMessageDialog(null, "Wprowadź Proszę liczbę całkowitą, a nie jakieś zabawy urządzasz!");
                }
            }
        });
    }

}
