package Services.GUI.impl.ProductsView;

import Model.GUI.AppWindow;
import Model.GUI.impl.SectionProductView;
import Model.Warehouse.Category;
import Model.Warehouse.Product;
import Services.GUI.api.SectionService;
import Services.GUI.impl.AppWindowService;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class SectionProductViewService extends SectionService {
    private SectionProductView sectionProductView = new SectionProductView();

    public void setPanel(AppWindow appWindow) {
        GridBagConstraints gbc = new GridBagConstraints();

        JPanel jPanel = appWindow.getjPanel();
        jPanel.removeAll();
        jPanel.repaint();
        jPanel.setLayout(new BorderLayout(10,10));
        jPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        this.setCategories(this.sectionProductView);
        jPanel.add(this.sectionProductView.getCategoryListPanel(), BorderLayout.NORTH);

        this.setProductsTable(sectionProductView);
        jPanel.add(sectionProductView.getListScroller(), BorderLayout.CENTER);

        jPanel.add(this.sectionProductView.getBackButton(), BorderLayout.SOUTH);

        appWindow.setjPanel(jPanel);

        this.setActions(appWindow, this.sectionProductView);
    }

    private void setProductsTable(SectionProductView sectionProductView){
        JTable productsTable = new JTable();
        Category selectedCategory = this.getSelectedCategory(sectionProductView);
        List<Product> productList = new ArrayList<>();

        if(selectedCategory!=null) {
            productList = this.getProductsByCategoryId(selectedCategory.getId());
        }

        DefaultTableModel model = new DefaultTableModel(sectionProductView.getColumnNames(),0);
        productsTable.setModel(model);
        productsTable = this.setProductsTableView(productsTable);
        for(Product product: productList){
            model.addRow(
                    new Object[] {
                            product.getName(), product.getValue(),
                            product.getPrice().toString().replace(".",",") + "zł"
                    }
            );
        }
        sectionProductView.setModel(model);
        sectionProductView.setProductsTable(productsTable);

        JScrollPane listScroller = sectionProductView.getListScroller();
        listScroller.setViewportView(productsTable);
        sectionProductView.setListScroller(listScroller);
    }

    private JTable setProductsTableView(JTable productsTable){
        productsTable.setFillsViewportHeight(true);
        productsTable.setBackground(this.sectionProductView.getSegmentColor());
        productsTable.setAutoCreateRowSorter(true);
        productsTable.setEnabled(false);
        productsTable.getTableHeader().setBackground(AppWindow.mainColor);

        productsTable.getTableHeader().setReorderingAllowed(false);
        productsTable.getColumnModel().getColumn(0).setPreferredWidth(400);
        productsTable.getColumnModel().getColumn(1).setPreferredWidth(80);
        productsTable.getColumnModel().getColumn(2).setMinWidth(70);

        return productsTable;
    }

    private void setActions(final AppWindow appWindow, final SectionProductView sectionProductView){
        final SectionProductViewService sectionProductViewService = new SectionProductViewService();

        for(JRadioButton category: sectionProductView.getCategoryList()){
            category.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    sectionProductViewService.setProductsTable(sectionProductView);
                }
            });
        }

        sectionProductView.getBackButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AppWindowService appWindowService = new AppWindowService(appWindow);
                appWindowService.setMainMenuWindow();
            }
        });
    }
}
