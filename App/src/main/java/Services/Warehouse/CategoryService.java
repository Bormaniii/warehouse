package Services.Warehouse;

import DAO.Warehouse.CategoryDAO;
import Model.Warehouse.Category;

import java.util.List;

public class CategoryService {
    private CategoryDAO categoryDAO = new CategoryDAO();


    public Category getCategoryById(int id) {
        return this.categoryDAO.getCategoryById(id);
    }

    public Category getCategoryByName(String name){
        return this.categoryDAO.getCategoryByName(name);
    }

    public boolean addNewCategory(String name) {
        return this.categoryDAO.addNewCategory(name);
    }

    public boolean addNewCategory(Category category) {
        return this.categoryDAO.addNewCategory(category);
    }

    public boolean updateCategory(Category category) {
        return this.categoryDAO.updateCategory(category);
    }

    public boolean deleteCategory(Category category){
        return this.categoryDAO.deleteCategory(category);
    }

    public List<Category> getEnableCategories() {
        return this.categoryDAO.getEnableCategories();
    }

}
