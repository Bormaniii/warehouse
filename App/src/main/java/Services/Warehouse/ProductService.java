package Services.Warehouse;

import DAO.Warehouse.ProductDAO;
import Model.Warehouse.Product;

import java.util.List;

public class ProductService {
    private ProductDAO productDAO = new ProductDAO();


    public Product getProductById(int id) {
        return productDAO.getProductById(id);
    }

    public Product getProductByName(String name){return  productDAO.getProductByName(name); }

    public boolean addNewProduct(String name, int categoryId, Double price){
        return productDAO.addNewProduct(name, categoryId, price);
    }

    public boolean addNewProduct(Product product){
        return productDAO.addNewProduct(product);
    }

    public boolean updateProduct(Product product){
        return productDAO.updateProduct(product);
    }

    public List<Product> getProductsByCategoryId(int idToFind){
        return this.productDAO.getProductsByCategoryId(idToFind);
    }

    public boolean deleteProduct(Product product){
        return this.productDAO.deleteProduct(product);
    }

    public boolean deleteProductByCategoryId(int idToFind){
        List<Product> products = this.getProductsByCategoryId(idToFind);
        return this.productDAO.deleteProductByCategoryId(products);
    }

    public List<Product> getAllProducts() {
        return this.productDAO.getAllProducts();
    }

}
