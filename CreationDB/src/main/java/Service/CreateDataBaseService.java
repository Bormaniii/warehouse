package Service;

import org.apache.ibatis.jdbc.ScriptRunner;

import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;

public class CreateDataBaseService {

    public void setDB() throws Exception  {

        //Registering the Driver
        DriverManager.registerDriver(new org.h2.Driver());
        //Getting the connection
        String mysqlUrl = "jdbc:h2:~/warehouse/warehouse";
        Connection con = DriverManager.getConnection(mysqlUrl, "admin", "admin");
        System.out.println("Connection established......");
        //Initialize the script runner
        ScriptRunner sr = new ScriptRunner(con);
        //Creating a reader object
        InputStream in = getClass().getResourceAsStream("/create.sql");
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        //Running the script
        sr.runScript(reader);
    }
}